export type SinglePersonInfoProps = {
  person: {
    created?: string | undefined;
    name?: string;
    birth_year?: string;
    gender?: string;
    height?: string;
    skin_color?: string;
    hair_color?: string;
    eye_color?: string;
  };
  handlePersonInfo: (e: string) => void;
};
