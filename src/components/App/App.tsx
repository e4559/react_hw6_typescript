import React from "react";
import Header from "../Header";
import MainPage from "../MainPage";
import Profile from "../Profile";
import styles from "./App.module.css";

function App(): JSX.Element {
    return (
        <>
            <Header />
            <div className={styles.container}>
                <Profile />
                <MainPage />
            </div>
        </>
    );
}

export default App;
