import React from "react";
import styles from "./Header.module.css";

function Header(): JSX.Element {
    return (
        <header>
            <div className={styles.container}>Header</div>
        </header>
    );
}

export default Header;
