export type PaginationProps = {
  changePage: (e: number) => void;
  i: number;
  currentPage: number;
};
