export type defaultStateType = {
  currentPage: number;
  count: number;
  results: { [key: string]: string }[];
};

export type singlePersonInfoType = {
  status: boolean;
  // person: {
  //   [key: string]: string;
  // };
  person: {
    created?: string;
    name?: string;
    birth_year?: string;
    gender?: string;
    height?: string;
    skin_color?: string;
    hair_color?: string;
    eye_color?: string;
  };
};

// export type stateType = {
//   currentPage: number;
//   count: number;
//   next: string | null;
//   previous: string | null;
//   results: {
//     created?: string;
//     name?: string;
//     birth_year?: string;
//     gender?: string;
//     height?: string;
//     skin_color?: string;
//     hair_color?: string;
//     eye_color?: string;
//   }[];
// };
