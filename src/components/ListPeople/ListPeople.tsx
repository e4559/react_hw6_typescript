import React, { useEffect, useState } from "react";
import { getData } from "../../api";
import { BASE_URL_PEOPLE } from "../../constants";
import Pagination from "../Pagination";
import SinglePersonInfo from "../SinglePersonInfo/SinglePersonInfo";
import SinglePerson from "../SinglePerson";
import styles from "./ListPeople.module.css";
import { defaultStateType, singlePersonInfoType } from "./List.props";

const defaultState = {
  currentPage: 1,
  count: 0,
  results: [],
};

function ListPeople(): JSX.Element {
  const [state, setState] = useState<defaultStateType>(defaultState);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [singlePersonInfo, setSinglePersonInfo] =
    useState<singlePersonInfoType>({
      status: false,
      person: {},
    });

  useEffect(() => {
    getData(`${BASE_URL_PEOPLE}${state.currentPage}`).then((data) => {
      setState(() => ({ ...data, currentPage: state.currentPage }));
      setIsLoading(false);
    });
  }, [state.currentPage]);

  const count = Math.ceil(state.count / 10);

  const handlePersonInfo = (id: string) => {
    const { results } = state;
    results.map((el) => {
      if (el.created === id) {
        return setSinglePersonInfo({
          status: !singlePersonInfo.status,
          person: el,
        });
      }
    });
  };

  const changePage = (page: number) => {
    setState(() => ({ ...state, currentPage: page }));
  };

  return (
    <>
      {isLoading ? (
        <div>Loading..</div>
      ) : singlePersonInfo.status ? (
        <SinglePersonInfo
          person={singlePersonInfo.person}
          handlePersonInfo={handlePersonInfo}
        />
      ) : (
        <>
          <div className={styles.container}>
            {state.results.map((el) => (
              <SinglePerson
                key={el.created}
                handlePersonInfo={handlePersonInfo}
                created={el.created}
                name={el.name}
              />
            ))}
          </div>
          <div className={styles.pagination}>
            {Array(count + 4)
              .fill(null)
              .map((el, i) => (
                <Pagination
                  key={i}
                  changePage={changePage}
                  currentPage={state.currentPage}
                  i={i}
                />
              ))}
          </div>
        </>
      )}
    </>
  );
}

export default ListPeople;
