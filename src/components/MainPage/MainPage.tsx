import React from "react";
import ListPeople from "../ListPeople/ListPeople";
import styles from "./MainPage.module.css";

function MainPage(): JSX.Element {
    return (
        <div className={styles.container}>
            {/* <button>People</button> */}
            <ListPeople />
        </div>
    );
}

export default MainPage;
