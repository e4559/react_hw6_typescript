import React from "react";
import styles from "./SinglePerson.module.css";
import { SinglePersonProps } from "./SinglePerson.props";

const SinglePerson: React.FC<SinglePersonProps> = ({
    created,
    name,
    handlePersonInfo,
}): JSX.Element => {
    // const createdStr: string = created !== undefined ? created : "";

    return (
        <div
            className={styles.container}
            onClick={() => handlePersonInfo(created)}
        >
            <p>{name}</p>
        </div>
    );
};

export default SinglePerson;
