export type SinglePersonProps = {
  created: string;
  name: string;
  handlePersonInfo: (e: string) => void;
};
