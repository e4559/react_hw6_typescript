export const getData = (url: string) => {
    return fetch(url)
        .then((res) => res.json())
        .then((data) => data)
        .catch((err) => err);
};
